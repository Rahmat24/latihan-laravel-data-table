@extends('layout.master')

@section('judul')
Halaman Table Data
@endsection

@section('content')


<a href="/cast/create" class="btn btn-primary mb-3">Tambah Cast</a>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($Cast as $key => $item)
            <tr>
                <td>{{$key +1}}</td>
                <td>{{$item->nama}}</td>
                <td>{{$item->umur}}</td>
                <td>{{$item->bio}}</td>
                <td>               
                    <form action="/cast/{{$item->id}}" method="POST">
                        <a href="/cast/{{$item->id}}" class="btn btn-info btm-sm">Detail</a>
                        <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btm-sm">Edit</a>
                        
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger btm-sm" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <h1>Data tidak ada</h1>
        @endforelse
      {{-- <tr>
        <th scope="row">1</th>
        <td>Mark</td>
        <td>Otto</td>
        <td>@mdo</td>
      </tr>
      <tr>
        <th scope="row">2</th>
        <td>Jacob</td>
        <td>Thornton</td>
        <td>@fat</td>
      </tr>
      <tr>
        <th scope="row">3</th>
        <td>Larry</td>
        <td>the Bird</td>
        <td>@twitter</td>
      </tr> --}}
    </tbody>
  </table>
@endsection