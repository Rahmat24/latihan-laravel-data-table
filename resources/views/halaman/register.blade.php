@extends('layout.master')

@section('judul')
Halaman Form
@endsection

@section('content')
<h1>Buat Account Baru</h1>
<form action="/welcome" method="post">
    @csrf
    <label for="">First Name : </label> <br><br>
    <input type="text" name="FirstName" id=""> <br><br>
    <label for="">Last Name : </label> <br><br>
    <input type="text" name="LastName" id=""> <br><br>
    
    <label for="">Gender </label> <br><br>
    <input type="radio" name="gender" id=""> Male <br>
    <input type="radio" name="gender" id=""> Female <br><br>
    
    <label for="">Nationality </label> <br><br>
    <select name="" id="">
        <option value="1">Indonesia</option>
        <option value="2">Amerika</option>
        <option value="3">Inggris</option>
    </select>
    <br><br>

    <label for="">Language Spoken </label> <br><br>
    <input type="checkbox" name="language" id=""> Bahasa Indonesia <br>
    <input type="checkbox" name="language" id=""> English <br>
    <input type="checkbox" name="language" id=""> Others <br>
    <br><br>

    <label for="">Bio </label> <br><br>
    <textarea name="bio" id="" cols="60" rows="10"></textarea>
    <br><br>



    <input type="submit" name="" value="kirim" id=""> <br><br>
    
</form>

@endsection
