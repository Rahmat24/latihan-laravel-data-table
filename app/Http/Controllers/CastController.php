<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cast;

class CastController extends Controller
{
    public function create(){
        return view('cast.create');
    }

    public function store(Request $request){
        $request->validate([
    		'nama' => 'required',
    		'bio' => 'required',
            'umur' => 'required',
    	],
        [
    		'nama.required' => 'Nama harus di isi',
    		'bio.required' => 'Bio harus di isi',
            'umur.required' => 'Umur harus di isi',
    	]);

        $Cast = new Cast;

        $Cast ->nama = $request->nama;
        $Cast ->umur = $request->umur;
        $Cast ->bio = $request->bio;

        $Cast ->save();

        return redirect('/cast');
    }

    public function index(){
        $Cast = Cast::all();
        return view('cast.index', compact('Cast'));
    }

    public function show($cast_id){
        $Cast = Cast::where('id',$cast_id)-> first();
        return view('cast.show', compact('Cast'));
    }

    public function edit($cast_id){
        $Cast = Cast::where('id',$cast_id)-> first();
        return view('cast.edit', compact('Cast'));
    }

    public function update(Request $request, $cast_id){
        $request->validate([
    		'nama' => 'required',
    		'bio' => 'required',
            'umur' => 'required',
    	],
        [
    		'nama.required' => 'Nama harus di isi',
    		'bio.required' => 'Bio harus di isi',
            'umur.required' => 'Umur harus di isi',
    	]);

        $Cast = Cast::find($cast_id);
        
        $Cast ->nama = $request['nama'];
        $Cast ->umur = $request['umur'];
        $Cast ->bio = $request['bio'];

        $Cast ->save();
        
        return redirect('/cast');
    }

    public function destroy($cast_id){
        $Cast = Cast::find($cast_id);
        $Cast ->delete();;
        return redirect('/cast');
    }

}
